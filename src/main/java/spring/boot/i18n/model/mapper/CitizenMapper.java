package spring.boot.i18n.model.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;
import spring.boot.i18n.dao.entity.CitizenEntity;
import spring.boot.i18n.model.dto.CitizenDto;

@Mapper
public interface CitizenMapper{
CitizenMapper INSTANCE = Mappers.getMapper(CitizenMapper.class);
@Mapping(source = "citizenDto.fin", target = "passport.fin")
@Mapping(source = "citizenDto.passportNumber", target = "passport.passportNumber")
CitizenEntity mapToEntity(CitizenDto citizenDto);
@Mapping(source = "passport.fin", target = "fin")
@Mapping(source = "passport.passportNumber", target = "passportNumber")
CitizenDto mapToDto(CitizenEntity citizenEntity);

}
