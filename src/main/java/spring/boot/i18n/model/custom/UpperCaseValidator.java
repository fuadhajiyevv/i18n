package spring.boot.i18n.model.custom;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

public class UpperCaseValidator implements ConstraintValidator<UpperCase, String> {
    @Override
    public void initialize(UpperCase constraintAnnotation) {
        ConstraintValidator.super.initialize(constraintAnnotation);
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        if(value == null){
            throw new NullPointerException();
        }
        char letter = value.charAt(0);
        return Character.isUpperCase(letter);
    }
}
