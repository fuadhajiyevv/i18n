package spring.boot.i18n.model.custom;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;

import java.lang.annotation.*;

@Documented
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = UpperCaseValidator.class)


public @interface UpperCase {

    String message() default "invalid.format";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}

