package spring.boot.i18n.model.dto;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Data;
import spring.boot.i18n.model.custom.UpperCase;

@Data
public class CitizenDto {

    @UpperCase
    @NotEmpty(message = "not.empty")
    private String firstName;
    @UpperCase
    @NotEmpty(message = "not.empty")
    private String lastName;
    @NotEmpty(message = "not.empty")
    @Size(min = 7,max = 7)
    private String fin;
    @NotNull
    private Integer passportNumber;
}
