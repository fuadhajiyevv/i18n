package spring.boot.i18n.dao.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "passport")
public class PassportEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer Id;

    @Column(name = "fin")
    private String fin;

    @Column(name = "passport_number")
    private Integer passportNumber;

    @OneToOne(mappedBy = "passport",cascade = CascadeType.ALL)
    private CitizenEntity citizenEntity;




}
