package spring.boot.i18n.dao.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import spring.boot.i18n.dao.entity.PassportEntity;

@Repository
public interface PassportRepository extends JpaRepository<PassportEntity,Integer> {
}
