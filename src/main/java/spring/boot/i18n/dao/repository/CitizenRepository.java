package spring.boot.i18n.dao.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import spring.boot.i18n.dao.entity.CitizenEntity;
@Repository
public interface CitizenRepository extends JpaRepository<CitizenEntity,Integer> {
}
