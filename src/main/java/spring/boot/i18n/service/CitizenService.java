package spring.boot.i18n.service;

import spring.boot.i18n.model.dto.CitizenDto;

import java.util.List;

public interface CitizenService {
    void create(CitizenDto studentDto);

    CitizenDto getById(Integer id);

    List<CitizenDto> getAll();

    void deleteById(Integer Id);
}
