package spring.boot.i18n.service.impl;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import spring.boot.i18n.dao.entity.CitizenEntity;
import spring.boot.i18n.dao.repository.CitizenRepository;
import spring.boot.i18n.model.dto.CitizenDto;
import spring.boot.i18n.model.mapper.CitizenMapper;

import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
@Slf4j
public class CitizenServiceImpl {
    private final CitizenRepository citizenRepository;
    private final MessageSource messageSource;

    public List<CitizenDto> findAll() {
        return citizenRepository.findAll()
                .stream()
                .map(CitizenMapper.INSTANCE::mapToDto)
                .collect(Collectors.toList());
    }

    public void save(CitizenDto citizenDto) {
        Locale getLocale = LocaleContextHolder.getLocale();
        String messageSource1 = messageSource.getMessage("successful.save",null,getLocale);
        log.info(messageSource1);
        CitizenEntity entity = CitizenMapper.INSTANCE.mapToEntity(citizenDto);
        citizenRepository.save(entity);

    }

    public Optional<CitizenDto> findById(Integer id) {
        Locale userLocale = LocaleContextHolder.getLocale();
        Object[] object = new Object[2];
        if(citizenRepository.findById(id).isPresent()) {
            object[0] = citizenRepository.findById(id).get().getFirstName();
            object[1] = citizenRepository.findById(id).get().getLastName();
        }else throw new NullPointerException();
        String message = messageSource.getMessage("greeting",object,userLocale);
        log.info(message);
        Optional<CitizenEntity> citizenEntityOptional = citizenRepository.findById(id);
        return citizenEntityOptional.map(CitizenMapper.INSTANCE::mapToDto);
    }


    public void deleteById(Integer integer) {
        Locale userLocale = LocaleContextHolder.getLocale();
        Object[] object = new Object[2];
        if(citizenRepository.findById(integer).isPresent()) {
            object[0] = citizenRepository.findById(integer).get().getFirstName();
            object[1] = citizenRepository.findById(integer).get().getLastName();
        }else throw new NullPointerException();
        String message = messageSource.getMessage("successful.delete",object,userLocale);
        log.info(message);
        citizenRepository.deleteById(integer);
    }
}
