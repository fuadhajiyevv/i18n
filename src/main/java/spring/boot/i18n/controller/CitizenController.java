package spring.boot.i18n.controller;


import jakarta.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import spring.boot.i18n.model.dto.CitizenDto;
import spring.boot.i18n.service.impl.CitizenServiceImpl;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/citizen")
public class CitizenController {

    private final CitizenServiceImpl citizenService;

    public CitizenController(CitizenServiceImpl citizenService) {
        this.citizenService = citizenService;
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Optional<CitizenDto> getById(@PathVariable(name = "id") Integer id){
        return citizenService.findById(id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.OK)
    public void create(@Valid @RequestBody CitizenDto citizenDto){
        citizenService.save(citizenDto);
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<CitizenDto> getAll(){
     return citizenService.findAll();
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void deleteById(@PathVariable(name = "id") Integer id){
        citizenService.deleteById(id);
    }
}
